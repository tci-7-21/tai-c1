// Eventos

document.getElementById('btnCargar').addEventListener('click', cargarRazas);
document.getElementById('btnImagen').addEventListener('click', cargarImagen);

// Funciones

function cargarRazas() {
    const res = document.getElementById('selRaza');
    res.innerHTML = '';
    fetch('https://dog.ceo/api/breeds/list')
    .then(response => response.json())
    .then(data => {
        if (data.status === 'success') {
            const razas = data.message;
            razas.forEach(raza => {
                const option = document.createElement('option');
                option.value = raza;
                option.text = raza;
                res.appendChild(option);
            });
        } else {
            console.error('Error al obtener las razas de perros.');
        }
    })
    .catch(error => {
        console.error('Error en la solicitud Fetch:', error);
    });
}

function cargarImagen() {
    const res2 = document.getElementById('selRaza');
    const razaCanina = res2.value;
    if (razaCanina){
        const imagenContainer = document.getElementById('imagenContainer');
        imagenContainer.innerHTML = '';
        axios.get(`https://dog.ceo/api/breed/${razaCanina}/images/random`)
        .then(response => {
            const imageUrl = response.data.message;
            const imagen = document.createElement('img');
            imagen.src = imageUrl;
            imagen.alt = razaCanina;
            imagenContainer.appendChild(imagen);
        })
        .catch(error => {
            console.error('Error en la solicitud Axios:', error);
        });
    } else {
        console.error('Por favor, selecciona una raza antes de cargar la imagen.');
    }
  }