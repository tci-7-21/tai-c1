function cargarDatos(){
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/albums"

    // Realizar funcion de respuesta de peticion

    http.onreadystatechange = function(){

        // Validar respuesta

        if (this.status == 200 && this.readyState == 4){
            let res = document.getElementById('lista');
            const json = JSON.parse(this.responseText);

            // Ciclo para mostrar los datos en la tabla

            for(const datos of json){
                res.innerHTML += '<tr> <td class="columna1">' + datos.userId + '</td>'
                + '<td class="columna2">' + datos.id + '</td>'
                + '<td class="columna3">' + datos.title + '</td> </tr>'
            }
        } // else alert("Surgio un error al hacer la peticion")
    }
    http.open('GET',url,true);
    http.send();
}

// Codificar los botones

document.getElementById("btnCargar").addEventListener('click',function(){
    cargarDatos();
});

document.getElementById("btnLimpiar").addEventListener('click',function(){
    let res = document.getElementById('lista');
    res.innerHTML="";
});