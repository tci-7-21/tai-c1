const llamandoFetch = () => {
    const url = "https://jsonplaceholder.typicode.com/users";
    fetch(url)
    .then(respuesta => respuesta.json())
    .then(respuesta => mostrarDatos(respuesta))
    .catch((reject) => {
        const lblError = document.getElementById('lblError');
        lblError.innerHTML = "Surgió un error: " + reject;
    });
};

const mostrarDatos = (data) => {
    const res = document.getElementById('lista');
    res.innerHTML = "";
    for (let item of data) {
        res.innerHTML += '<tr><td class = "columna0">' + item.id + '</td>'
        + '<td class = "columna1">' + item.name + '</td>'
        + '<td class = "columna2">' + item.email + '</td>'
        + '<td class = "columna3">' + item.address.street + '</td>'
        + '<td class = "columna4">' + item.address.suite + '</td>'
        + '<td class = "columna5">' + item.address.city + '</td>'
        + '<td class = "columna6">' + item.address.zipcode + '</td>'
        + '<td class = "columna7">' + item.address.geo.lat + '</td>'
        + '<td class = "columna8">' + item.address.geo.lng + '</td>'
        + '<td class = "columna9">' + item.phone + '</td>'
        + '<td class = "columna10">' + item.website + '</td>'
        + '<td class = "columna11">' + item.company.name + '</td>'
        + '<td class = "columna12">' + item.company.catchPhrase + '</td>'
        + '<td class = "columna13">' + item.company.bs + '</td></tr>';
    };  
}

document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});

document.getElementById('btnCargar').addEventListener('click',function(){
    llamandoFetch();
});