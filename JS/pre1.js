
// Declaracion de constantes

const btnBuscar = document.getElementById("btnBuscar");
const btnLimpiar = document.getElementById("btnLimpiar");
const id = document.getElementById("txtID");
const lista = document.getElementById("lista");

// Funciones

function buscarUsuario() {
    const userId = id.value.trim();

    if (userId == "") {
        alert("Inserte un ID");
        return;
    }

    axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`)
        .then(response => {
            const vendedor = response.data;
            lista.innerHTML = mostrarDatos(vendedor);
        })
        .catch(error => {
            manejarError("No existe este ID");
        });
}

function mostrarDatos(vendedor) {
    return `
        <tr>
            <td>${vendedor.id}</td>
            <td>${vendedor.name}</td>
            <td>${vendedor.username}</td>
            <td>${vendedor.email}</td>
            <td> Calle: ${vendedor.address.street} <br> 
                 Número: ${vendedor.address.suite} <br> 
                 Ciudad: ${vendedor.address.city} </td>
        </tr>`;
}

function manejarError(mensaje) {
    console.error("Error al hacer la petición:", mensaje);
    alert(mensaje);
}

// Eventos

btnBuscar.addEventListener("click", buscarUsuario);

btnLimpiar.addEventListener("click", function () {
    id.value = "";
    lista.innerHTML = "";
});