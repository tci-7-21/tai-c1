// Declaración de variables
const btnBuscar = document.getElementById("btnBuscar");
const btnLimpiar = document.getElementById("btnLimpiar");

const pais = document.getElementById("txtPais");
const resultado = document.getElementById("resultado");
const capital = document.getElementById("capital");
const lenguaje = document.getElementById("lenguaje");

btnBuscar.addEventListener("click", function () {
    const paisBusqueda = pais.value.trim();
    if (paisBusqueda != "") {
        fetch(`https://restcountries.com/v3.1/name/`+paisBusqueda)
            .then(response => response.json())
            .then(data => {
                if (data.length > 0) {
                    const paisInfo = data[0];
                    capital.textContent = `Capital: ${paisInfo.capital}`;

                    const languages = Object.values(paisInfo.languages).join(', ');
                    lenguaje.textContent = `Lenguajes: ` +languages;

                    resultado.style.display = "block";
                } else {
                    resultado.style.display = "none";
                    alert("No se encontró información...");
                }
            })
            .catch(error => {
                console.error("Error en la petición:", error);
            });
    } else {
        alert("Ingresa un país...");
    }
});

btnLimpiar.addEventListener("click", function () {
    lenguaje.innerHTML = "Lenguaje: ";
    capital.innerHTML = "Capital: ";
    pais.value = "";
});